from Log_Handler import Log_Handler as lh
import inspect, json
from concurrent.futures import ThreadPoolExecutor
import argparse
import requests
from lxml import etree, builder 
import pymongo

error_logger = lh.log_initializer("error_log.log")


'''
>> Method Description: 
::	
>> Params:
::	
'''
#start
	# method_name = inspect.stack()[0][3]
	# return_dict = {
	# "return_flag" : False
	# }
	# try:


	# 	return_dict["return_flag"] = True
	# 	return return_dict
	# except Exception as Ex:
	# 	error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
	# 	return return_dict
#end

'''
>> Method Description: 
::	This method manages the flow of execution. It calls the respective method for 
	each step in the flow, captures the return value and passes it to the next step 
	after checking if the step executed as expected 
>> Params:
::	No Params
'''
def manage_execution():
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		#Step 1: take command line inputs 
		in_return = get_return_inputs()
		if not in_return["return_flag"]: 
			print("Step 1 Failed. Please Check Logs")
			return return_dict
		print("Step 1 complete. Input Registered.")

		#Step 2: Http Post requests 
		print("Step 2 started. Getting data for city: {0} | checkin: {1} | checkout: {2}".format(
			in_return["city"], in_return["checkin"], in_return["checkout"]))

		api_return = prepare_post_api_data(in_return["city"], in_return["checkin"], in_return["checkout"])
		if not api_return["return_flag"]: 
			print("Step 2 Failed. Please Check Logs")
			return return_dict
		print("Step 2 complete. Data successfully fetched.")

		#Step 3.1: Parse process responses 
		print("Step 3 started. Parsing and processing data")
		parse_return = parse_process_response(api_return["response_list"])
		if not parse_return["return_flag"]: 
			print("Execution Failed. Please Check Logs")
			return return_dict

		#Step 3.2: Dump into db
		print("Inserting data in DB.")
		db_return = insert_into_db(parse_return["common_hotel_details"])
		if not db_return["return_flag"]: 
			print("Execution Failed. Please Check Logs")
			return return_dict
		print("Step 3 complete. Data successfully inserted.")

		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict


'''
>> Method Description: 
::	This method collects the input from the command line argument and returns it back 
	the captured inputs
>> Params:
::	No Params
'''
def get_return_inputs():
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument('--city', help='city name')
		parser.add_argument('--checkin', help='Checkin')
		parser.add_argument('--checkout', help='Checkout')
		args = parser.parse_args()

		return_dict["city"] = args.city
		return_dict["checkin"] = args.checkin
		return_dict["checkout"] = args.checkout

		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict

'''
>> Method Description: 
::	This method prepares the required data for the post call and calls the APIs asynchronously
>> Params:
::	city: city name collected from the input 
::	checkin: checkin date time collected from the input 
:: 	checkout: checkout date time collected from the input 
'''
def prepare_post_api_data(city, checkin, checkout):
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		json_provider = "snaptravel"
		json_url = "https://experimentation.getsnaptravel.com/interview/hotels"
		json_body = {
				  "city" : city,
				  "checkin" : checkin,
				  "checkout" : checkout,
				  "provider" : json_provider
				}

		xml_provider = "retail"
		xml_url = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"
		xml_body = etree.tostring(
					builder.E.root(
						builder.E.checkin(checkin),
						builder.E.checkout(checkout),
						builder.E.city(city),
						builder.E.provider(xml_provider)

					), pretty_print=True, xml_declaration=True, encoding='UTF-8')

		#asynchrnous requests
		with ThreadPoolExecutor(max_workers=10) as pool:
			response_list = pool.map(do_post, [
				    			{
				    				"type": "json",
				    				"provider": json_provider,
				    				"url": json_url, 
				    				"body": json.dumps(json_body), 
				    				"headers":{'Content-Type':'application/json; charset=UTF-8'}
			    				}, 
				    			{
				    				"type": "xml",
				    				"provider": xml_provider,
				    				"url": xml_url, 
				    				"body": xml_body, 
				    				"headers":{'Content-Type':'application/xml; charset=UTF-8'}
			    				}
							])

		return_dict["response_list"] = response_list
		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict

'''
>> Method Description: 
::	This method makes the requested post to the API
>> Params:
::	request_details: Dictionary with details requires for making the post request
::		url: Post API url
::		data: data to be posted (body)
::		headers: headers contained content type and charset 
:: 		type: json or xml 
::		provider: service provider (e.g.: snaptravel, hotel.com) 
'''
def do_post(request_details):
	method_name = inspect.stack()[0][3]
	try:
		response = requests.post(url = request_details["url"], 
								data = request_details["body"], 
								headers = request_details["headers"])

		#adding type and provider to the response this because it 
		#would be required later when finding common hotels
		return {
			"type": request_details["type"],
			"provider": request_details["provider"],
			"status_code": response.status_code,
			"content": response.content
		}
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return False


'''
>> Method Description: 
::	Parse the list of responses received from the APIs and process them to find the 
	hotels common in the response of both the API request
>> Params:
::	response_list: list of all the responses received from the API call as return by 
	prepare_post_api_data method	
'''
def parse_process_response(response_list):
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		common_ids_list = []
		provider_hotel_details = []

		#looping through the list because we are unsure about the order of responses 
		for response in response_list:
			if response["type"] == "json":
				json_ids_return = get_json_hotel_ids(response)
				if not json_ids_return["return_flag"]: return return_dict
				json_hotel_details = json_ids_return["all_hotel_details"]
				json_hotel_ids = json_ids_return["hotel_ids"]
				json_hotel_provider = response["provider"]
			elif response["type"] == "xml":
				xml_ids_return = get_xml_hotel_ids(response)
				if not xml_ids_return["return_flag"]: return return_dict
				xml_hotel_details = xml_ids_return["all_hotel_details"]
				xml_hotel_ids = xml_ids_return["hotel_ids"]
				xml_hotel_provider = response["provider"]

		#finding the common ids between the two list 
		common_ids_list = list(set(json_hotel_ids).intersection(xml_hotel_ids))		
		
		common_hotel_details = []
		# looping through the common ids and finding its respective details in the 
		# json hotel details response. 
		for common_id in common_ids_list:
			hotel_details = json_hotel_details[common_id]
			json_provider_price = hotel_details.pop('price', None)
			xml_provider_price = xml_hotel_details[common_id]["price"]

			# replacing . and $ because they are not acceptable in the keys by Mongo
			# Although this has its own drawbacks, it would need to be discussed
			json_hotel_provider = json_hotel_provider.replace(".", "-").replace("$", "--")
			xml_hotel_provider = xml_hotel_provider.replace(".", "-").replace("$", "--")

			all_prices = {
				json_hotel_provider: json_provider_price,
				xml_hotel_provider: xml_provider_price
			}

			hotel_details["prices"] = all_prices
			common_hotel_details.append(hotel_details)
		
		# error_logger.info("common_hotel_details: {0}".format(common_hotel_details))

		return_dict["common_hotel_details"] = common_hotel_details
		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict


'''
>> Method Description: 
:: 	This method parses the responses from the JSON request and extracts all the hotel ids 
	along with the hotel details 		
>> Params:
::	response: Individial response from the JSON post request 
'''
def get_json_hotel_ids(response):
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		parsed_response = json.loads(response["content"])
		
		hotel_ids = []
		all_hotel_details = {}
		for hotel in parsed_response["hotels"]:
			hotel_id = int(hotel["id"])
			hotel_ids.append(hotel_id)

			# adding the hotel json object to its respective id in a python dictionary 
			# this would make it straight forward to loop up the details of common hotel ids 
			# without looping through all the details again 
			all_hotel_details[hotel_id] = hotel

		return_dict["all_hotel_details"] = all_hotel_details
		return_dict["hotel_ids"] = hotel_ids
		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict


'''
>> Method Description: 
:: 	This method parses the responses from the XML request and extracts all the hotel ids 
	along with the hotel details (only price) 		
>> Params:
::	response: Individial response from the XML post request
'''
def get_xml_hotel_ids(response):
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		parsed_response = etree.fromstring(response["content"])

		hotel_ids = []
		all_hotel_details = {}
		for hotel in parsed_response:
			hotel_id = int(hotel.find("id").text)
			hotel_ids.append(hotel_id)
			hotel_details_dict = {} 
			
			# only returning the price of hotels from XML as parsing other tags 
			# would require extra processing and based on the requirements it is assumed that 
			# the hotel details between JSON and XML response would be the same 
			# Therefore the details would be picked up from JSON response and only prices 
			# that are different can be used from hear 
			# Converting to float because .text turns the value into a string 
			hotel_details_dict["price"] = float(hotel.find("price").text)

			all_hotel_details[hotel_id] = hotel_details_dict
		return_dict["all_hotel_details"] = all_hotel_details
		return_dict["hotel_ids"] = hotel_ids
		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict

'''
>> Method Description: 
:: 	This method takes the list of hotels that are common between both the service providers
	and inserts it into the Mongo DB
>> Params:
::	hotel_details: List of common hotels along with its detail as received in the API response 
	along with prices from all service providers	
'''
def insert_into_db(hotel_details):
	method_name = inspect.stack()[0][3]
	return_dict = {
	"return_flag" : False
	}
	try:
		mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")
		db_object = mongo_client["snaptravel_demo"]
		col_object = db_object["hotel_details"]

		col_object.insert_many(hotel_details)
		return_dict["return_flag"] = True
		return return_dict
	except Exception as Ex:
		error_logger.error("Expcetion occurred in " + method_name + "| Exception:" + str(Ex))
		return return_dict

'''
>> Execution begings here 
>> In order to start the execution using the following command 
>> Optional: replace the "CityName", "CheckIn" and "Checkout" with actual bvalues 
$ python Connectivity_Demo.py --city "CityName" --checkin "CheckIn" --checkout "CheckOut"
'''
if __name__ == "__main__":
	manage_execution()



	
