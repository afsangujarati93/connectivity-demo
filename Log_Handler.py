# -*- coding: utf-8 -*-
import logging
import os


class Log_Handler:
    def log_initializer(file_name):
        log_folder = "_logs_"
        log_file_path = log_folder + "/" +file_name
        if not os.path.isdir(log_folder): 
            os.makedirs(log_folder)
        logger = logging.getLogger(log_file_path)        
        if not getattr(logger, 'handler_set', None):            
            hdlr = logging.FileHandler(log_file_path)
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr) 
            logger.setLevel(logging.DEBUG)
    #        logger = logging.getLogger(__name__)
            logger.propagate = False
            logger.handler_set = True
        return logger
    